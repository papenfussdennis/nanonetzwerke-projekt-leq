public class Binding {

    private int strength = 0;
    private String label = "";

    public Binding() {

    }

    public Binding(int strength, String label) {
        this.strength = strength;
        this.label = label;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
