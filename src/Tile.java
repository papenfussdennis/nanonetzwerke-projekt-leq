import java.util.ArrayList;

public class Tile {
    private String name;
    private Binding northBinding;
    private Binding eastBinding;
    private Binding southBinding;
    private Binding westBinding;
    private String tileColor = "white";
    private String textColor = "black";
    private static ArrayList<Tile> createdTiles = new ArrayList<>();

    public Tile(String name, Binding northBinding, Binding eastBinding,
                Binding southBinding, Binding westBinding,
                String tileColor, String textColor) {
        this.name = name;
        this.northBinding = northBinding;
        this.eastBinding = eastBinding;
        this.southBinding = southBinding;
        this.westBinding = westBinding;
        this.tileColor = tileColor;
        this.textColor = textColor;
        createdTiles.add(this);
    }

    public Tile(String name, Binding northBinding, Binding eastBinding,
                Binding southBinding, Binding westBinding) {
        this(name, northBinding, eastBinding, southBinding, westBinding,
                "white", "black");
    }

    public Tile(String name, Binding northBinding, Binding eastBinding,
                Binding southBinding, Binding westBinding, String tileColor) {
        this(name, northBinding, eastBinding, southBinding, westBinding,
                tileColor, "black");
    }

    public static void printAll() {
        StringBuilder json = new StringBuilder("{\"_tiles\":[");
        for (Tile tile : createdTiles) {
			json.append(tile.toString());
        }
        json.delete(json.length() - 2, json.length());
        json.append("\n]}");
        System.out.println(json);
    }

    @Override
    public String toString() {
        String s = "";
        s += "{\n";
        s += "\"label\": \"" + name + "\",\n";
        s += "\"glues\": [\n";
        s += "{\n";
        s += "\"label\": \""+northBinding.getLabel()+"\",\n";
        s += "\"strength\": "+northBinding.getStrength()+"\n";
        s += "},\n";
        s += "{\n";
        s += "\"label\": \""+eastBinding.getLabel()+"\",\n";
        s += "\"strength\": "+eastBinding.getStrength()+"\n";
        s += "},\n";
        s += "{\n";
        s += "\"label\": \""+southBinding.getLabel()+"\",\n";
        s += "\"strength\": "+southBinding.getStrength()+"\n";
        s += "},\n";
        s += "{\n";
        s += "\"label\": \""+westBinding.getLabel()+"\",\n";
        s += "\"strength\": "+westBinding.getStrength()+"\n";
        s += "}\n";
        s += "],\n";
        s += "\"color\": \""+tileColor+"\"\n";
        s += "},\n";
        return s;
    }

}
