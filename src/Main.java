public class Main {
    public static void main(String[] args) {
        String number1 = "0001";
        String number2 = "1110";

        Binding emptyBinding = new Binding();

        // This tile is for the seed
        Binding seedNorth = new Binding(3, "sn");
        Binding seedSouth = new Binding(3, "ss");
        Binding seedEast = new Binding(1, "se");
        Tile seed = new Tile("Seed", seedNorth, seedEast, seedSouth, emptyBinding, "red");

        // The two arrow tiles above and below the seed tile
        Binding inputALeft = new Binding(3, "ial");
        Binding inputBLeft = new Binding(3, "ibl");
        Tile topArrowTile = new Tile("TA", emptyBinding, inputALeft, seedNorth, emptyBinding,
                "purple");
        Tile bottomArrowTile = new Tile("BA", seedSouth, inputBLeft, emptyBinding, emptyBinding,
                "purple");


        // The indicator tiles between the two lines of input tiles
        Binding zeroTopBinding = new Binding(1, "t0");
        Binding oneTopBinding = new Binding(1, "t1");
        Binding zeroBottomBinding = new Binding(1, "b0");
        Binding oneBottomBinding = new Binding(1, "b1");

        Binding intermediateEqualBinding = new Binding(1, "eq");
        Binding intermediateDiffBinding = new Binding(1, "neq");

        // 3 Start Tiles
        Tile indicatorEqZeroStart = new Tile("ES0", zeroTopBinding, intermediateEqualBinding,
                zeroBottomBinding, seedEast, "green");
        Tile indicatorEqOneStart = new Tile("ES1", oneTopBinding, intermediateEqualBinding,
                oneBottomBinding, seedEast, "green");
        Tile indicatorSmStart = new Tile("DS", zeroTopBinding, intermediateDiffBinding,
                oneBottomBinding, seedEast, "green");

        // 3 Equals Tiles
        Tile indicatorEqZero = new Tile("Eq0", zeroTopBinding, intermediateEqualBinding,
                zeroBottomBinding, intermediateEqualBinding, "green");
        Tile indicatorEqOne = new Tile("Eq1", oneTopBinding, intermediateEqualBinding,
                oneBottomBinding, intermediateEqualBinding, "green");
        Tile indicatorSm = new Tile("Sm", zeroTopBinding, intermediateEqualBinding,
                oneBottomBinding, intermediateDiffBinding, "green");

        // 4 Diff Tiles
        Tile indicatorDiff00 = new Tile("D00", zeroTopBinding, intermediateDiffBinding,
                zeroBottomBinding, intermediateDiffBinding, "green");
        Tile indicatorDiff01 = new Tile("D01", zeroTopBinding, intermediateDiffBinding,
                oneBottomBinding, intermediateDiffBinding, "green");
        Tile indicatorDiff10 = new Tile("D10", oneTopBinding, intermediateDiffBinding,
                zeroBottomBinding, intermediateDiffBinding, "green");
        Tile indicatorDiff11 = new Tile("D11", oneTopBinding, intermediateDiffBinding,
                oneBottomBinding, intermediateDiffBinding, "green");

        // Tile indicatorEq = new Tile("Eq", )


        // The two stop tiles to the right of the input and the output tile
        Binding inputARight = new Binding(3, "iar");
        Binding inputBRight = new Binding(3, "ibr");
        Binding endTileTop = new Binding(1, "ett");
        Binding endTileBottom = new Binding(1, "etb");
        Binding outputRight = new Binding(1, "ur");
        Binding outputTop = new Binding(1, "ut");
        Binding outputBottom = new Binding(1, "ob");

        Tile stopTopTile = new Tile("ST", outputTop, emptyBinding, endTileTop, inputARight,
                "pink");
        Tile stopBottomTile = new Tile("SB", endTileBottom, emptyBinding, outputBottom, inputBRight,
                "pink");
        Tile endTileEq = new Tile("End", endTileTop, outputRight, endTileBottom, intermediateEqualBinding);
        Tile endTileSm = new Tile("End", endTileTop, outputRight, endTileBottom, intermediateDiffBinding);

        // The top input tiles.
        for(int i = 0; i < number1.length(); i++) {
            char bit1 = number1.charAt(i);
            Binding top = emptyBinding;
            Binding right = i == number1.length() - 1 ? inputARight : new Binding(3, "t" + i);
            Binding bottom = bit1 == '0' ? zeroTopBinding : oneTopBinding;
            Binding left = i == 0 ? inputALeft : new Binding(3, "t" + (i - 1));
			Tile topTile = new Tile("", top, right, bottom, left, "white");
        }

        // The bottom input tiles.
        for(int i = 0; i < number2.length(); i++) {
            char bit1 = number2.charAt(i);
            Binding top = bit1 == '0' ? zeroBottomBinding : oneBottomBinding;
            Binding right = i == number1.length() - 1 ? inputBRight : new Binding(3, "b" + i);
            Binding bottom = emptyBinding;
            Binding left = i == 0 ? inputBLeft : new Binding(3, "b" + (i - 1));
            Tile topTile = new Tile("", top, right, bottom, left, "white");
        }
        Tile.printAll();

    }

}
